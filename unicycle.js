function drawCounter() {
    document.getElementById('counter').innerText = document.body.dataset.counter;
}
function drawAddressJSON() {
    document.getElementById('addressJSON').innerText = document.address.dataset.json;
}
function drawFormErrors(errors) {
    return [
        ...Object.entries(errors).map(([field, error]) => () => document.address[field].dataset.error = error),
        () => document.address.send.disabled = Object.values(errors).some(Boolean)
    ];
}
// todo async validator
function validate(address) {
    const zipRegex = {
        Germany: /^\d{5}$/,
        Switzerland: /^\d{4}$/,
        Russia: /^\d{6}$/
    };
    const errors = {country: '', zip: '', street: ''};
    if (address.country in zipRegex && !address.zip.match(zipRegex[address.country])) {
        return Object.assign(errors, {zip: 'incorrect'});
    }
    if (!(address.country in zipRegex)) {
        return Object.assign(errors, {country: 'incorrect'});
    }
    if (!address.street) {
        return Object.assign(errors, {street: 'mandatory'});
    }
    return errors;
}
const eventHandlers = {
    'load body'() {
        const address = {
            street: document.address.street.value,
            zip: document.address.zip.value,
            country: document.address.country.value
        };
        return [
            () => document.body.dataset.counter = 1,
            drawCounter,
            () => document.address.dataset.json = JSON.stringify(address),
            ...drawFormErrors(validate(address)),
            drawAddressJSON
        ];
    },
    'click body'() {
        return [
            () => document.body.dataset.counter++,
            drawCounter
        ];
    },
    'keyup address'(e) {
        const address = JSON.parse(document.address.dataset.json);
        address[e.target.name] = e.target.value;
        return [
            () => document.address.dataset.json = JSON.stringify(address),
            ...drawFormErrors(validate(address)),
            drawAddressJSON
        ];
    }
};
function applyEffects(effects) {
    effects.forEach(e => e());
}
function bindEvents(events) {
    Object.keys(events).forEach(k => {
        const words = k.split(' ');
        const event = events[k];
	    document[words[1]][`on${words[0]}`] = (e) => applyEffects(event(e));
    });
}
bindEvents(eventHandlers);

/*
meaning behind the code:
we have 2 functions: apply effects (draw html) and process events (events => effects)
he hide state model: store it in dataset or in inputs or in dom and change it in effects
current limitation:
* event processors cannot depend on each other
* can't throw away dom
design decisions:
* effects should not fire events
features:
* ability to record, undo and replay events or effects
in ideal world:
event handlers get full model and event; return modified model; effects are calculated based on model diff
*/
